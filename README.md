XMLClassGenerator
=================

INTRODUCTION:
-------------

    This program is an open source, MIT, and near "from scratch" program, 
    to be used to generate a C# importer class from 
    a "complete" xml document schema.
    
    The code used is really simple and can be understood by anyone.

USAGE:
------

    (the executable can be found in bin/Debug/ feel free to rebuild for another
    target if necessary)

    To use:

```
    XMLClassGenerator.exe inputFilePath outputFilePath.cs DocumentTypeName
```
    (DocumentTypeName: the importer class will have this name 
    in the generated code.)

EXAMPLE:
--------

    An example is available under bin/Debug
    (but has allready be done once, so you can allready
    find the generated C# file in the same debug folder)
    
    the arguments to try it :
```
    bin/Debug/XMLClassGenerator.exe bin/Debug/monster.dae Collada.cs Collada
```    
    to use the importer class generated:
    
    add the generated Collada.cs file in your project.
```
    ColladaDocument doc = ColladaDocument.ImportFromFile("monster.dae");
    
    Console.WriteLine("Vertices:\n"
    doc.COLLADA.library_geometries.geometry.mesh.source.float_array.Text);
	Console.WriteLine(
	"Count:" +
	doc.COLLADA.library_geometries.geometry.mesh.source.float_array.count);
	Console.WriteLine("done");
```

ADDITIONAL TIPS:
----------------

-> multiple elements with same name:

	in the given example, you can access
	`.source` or `.sourceElementList` 

	
	`.sourceElementList` is a list in case there is more than one element 
	"source" in the "mesh" element.
	"source" will return the first element.

-> attribute named like an element:

	In case an attribute is sometime found and has the same name than an 
	existing element, the name will be replaced with an additional "_"
	(e.g.: "source" is sometime an attribute, and sometime an element,
	when i ll need a "source" attribute, it will be written 
	like that: "_source")
    
-> conflict with C# types:

    In case you got problem with some C# builtin type that is the name of an
    element or attribute,
    go in the Program.cs and add your type in the "BuiltinTypes" or in
    the "AdditionalTypes" static Dictionary.
    Any of this type found will be replaced like before (e.g.: "_float").
